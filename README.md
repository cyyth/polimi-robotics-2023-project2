# Project 2 (2022/2023)
## Prerequisites
After installing ROS Noetic, 

``` bash
apt-get install -y "ros-noetic-slam-toolbox-*" ros-noetic-amcl ros-noetic-move-base ros-noetic-teb-local-planner ros-noetic-base-local-planner "ros-noetic-stage-*" ros-noetic-global-planner ros-noetic-navfn ros-noetic-dwa-local-planner
```

## Mapping
### Requires
- slam_toolbox
- pointcloud_to_laserscan

### Launch
To launch, 

``` bash
roslaunch mapping.launch.xml bag_name:=<name-of-the-bag-file-without-file-extionsion>
```

When mapping we can set the rosparam "tf_publisher/is_2d" to true to project the t265 frame to the x-y plane. But for the version that will be handed-in I will comment out the bag replay node.

The laserscan topic can be switch in the ./cfg/slam_config between
- /scan
- /velodyne/scan (from pointcloud_to_laserscan)

## Navigation
### Requires
- amcl
- move_base
- teb_local_planner
- base_local_planner (optional)

### Uses
- the local planner uses teb_local_planner (default) or base_local_planner
- the global planner uses the default one (navfn/NavfnROS)
- amcl is used for localization

### Launch
To launch,

``` bash
roslaunch navigation.launch.xml map_file:=<path-to-yaml> planner_file:=<path-to-yaml> world_file:=<path-to-world>
```

All the <path> can be written as map-file:="$(rospack find second_project)/stage/bag2_scan_mp.yaml" or absolute path as well.
The map that is selected to use in Stage simulation is the second bag created with 2D LiDAR data.
The map frame (0,0) is located in the lower-left of the map.

To test the waypoints following functionality, run

``` bash
rosservice call /set_waypoints "$(rospack find second_project)/waypoints.csv"
```

### Waypoints
Included are 5 waypoints that had been tested to work. The waypoints are shown here 

![alt text](waypoints.png)

The service recieve the path to the CSV file and returns
- total number of waypoints (num_waypoints)
- total number of waypoints that robot reached (success_waypoints)
- boolean to tell weather the robot reached all the waypoints (success)

The service will discard the current waypoints if the robot takes more than given amount of time (in second).
This value can be set using rosparam (/waypoint_parser/exec_timeout).

### Maps

Included in the ./stage/, there are two maps the cleaned version, and very CLEAN version. Both are tested.

To run the cleaned map:

``` bash
roslaunch second_project navigation.launch.xml map_file:=$(rospack find second_project)/stage/bag2_scan_map.yaml world_file:=$(rospack find second_project)/stage/bag2.world
```

To run the super clean map (default):

``` bash
roslaunch second_project navigation.launch.xml map_file:=$(rospack find second_project)/stage/bag2_scan_map_clean.yaml world_file:=$(rospack find second_project)/stage/bag2_clean.world
```

### Local Planners
Here the global planner is the default one, but we can switch the local planner between
- teb_local_planner (default)
- base_local_planner

which is possible by running 

``` bash
roslaunch second_project navigation.launch.xml planner_file:=$(rospack find second_project)/cfg/planner.yaml
```

# Remarks
- On my system, RViz seems slow when Stage is launched unfocused. Focusing on the Stage window will solve the problem.
- We should wait until every node has started before calling the waypoints_parser service.

