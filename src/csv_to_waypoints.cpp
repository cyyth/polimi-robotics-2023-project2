#include "ros/ros.h"

// ROS Messages
#include "geometry_msgs/PoseStamped.h"
#include "tf/transform_datatypes.h"

// Services
#include "second_project/CsvToWaypoints.h"

// std libraries
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#define PI 3.14159265359

bool c2wServiceCallback(second_project::CsvToWaypoints::Request &req,
                        second_project::CsvToWaypoints::Response &res) {
  // Response placeholder
  std::vector<geometry_msgs::PoseStamped> waypoints;

  // Read the CSV file
  std::string csv_file = req.filename;
  std::ifstream file(csv_file);
  if (!file.is_open()) {
    ROS_ERROR_STREAM("Failed to open CSV file: " << csv_file);
    return true;
  }

  std::string line;
  int num_waypoints = 0;
  while (std::getline(file, line)) {
    num_waypoints++;
    std::istringstream csvss(line);
    std::string x_str, y_str, theta_str, tmp;
    double x, y, theta;

    ROS_INFO("line %d: %s", num_waypoints, line.c_str());
    std::getline(csvss, x_str, ',');
    std::getline(csvss, y_str, ',');
    std::getline(csvss, theta_str);

    try {
      x = std::stod(x_str);
      y = std::stod(y_str);
      theta = std::stod(theta_str) * PI / 180.0;
    } catch (const std::exception &e) {
      ROS_WARN_STREAM("Failed to parse line in CSV file: " << line);
      continue;
    }

    // Create a PoseStamped message
    geometry_msgs::PoseStamped waypoint;
    waypoint.header.stamp = ros::Time::now();
    waypoint.header.frame_id = "";
    waypoint.pose.position.x = x;
    waypoint.pose.position.y = y;
    waypoint.pose.position.z = 0.0;
    waypoint.pose.orientation = tf::createQuaternionMsgFromYaw(theta);

    waypoints.push_back(waypoint);
  }

  file.close();

  res.num_waypoints = num_waypoints;
  res.waypoints = waypoints;
  return true;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "csv_to_waypoints");
  ros::NodeHandle n;
  ros::ServiceServer c2w_server =
      n.advertiseService("csv_to_waypoints", &c2wServiceCallback);
  ROS_INFO("The CSV to waypoints service is ready.");

  ros::spin();

  return 0;
}
