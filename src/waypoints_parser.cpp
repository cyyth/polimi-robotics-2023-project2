#include "ros/ros.h"

// ROS Messages
#include "actionlib_msgs/GoalStatusArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_msgs/Header.h"
#include "tf/transform_datatypes.h"
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"

// Services
#include "second_project/CsvToWaypoints.h"
#include "second_project/SetWaypoints.h"

// std libraries
#include <cstdint>
#include <math.h>
#include <string>
#include <vector>

#define PI 3.14159265359

class WaypointsParser {
private:
  ros::NodeHandle n_;
  ros::ServiceServer set_waypoints_server_;
  ros::ServiceClient c2w_client_;
  ros::Subscriber move_base_status_subscriber_;
  ros::Publisher move_base_goal_publisher_;
  ros::Publisher goal_markers_publisher_;

  second_project::CsvToWaypoints c2w_srv_;

  ros::Duration goal_pub_delay_; // delay before publishing the next goal
  int status_ = 0;               // a copy of ROS move_base status
  int exec_timeout_ = 60;        // time for the robot to execute the trajectory
  std::string c_frame_id_;       // the goal frame id
  uint32_t marker_shape_ = visualization_msgs::Marker::ARROW;

  void _showMarkers(second_project::CsvToWaypoints::Response &response,
                    uint8_t show) {
    // Visualize all the waypoints
    visualization_msgs::MarkerArray goals;
    for (unsigned int i = 0; i < c2w_srv_.response.num_waypoints; i++) {
      visualization_msgs::Marker goal;
      goal.header.frame_id = c_frame_id_;
      goal.header.stamp = ros::Time::now();
      goal.id = i;
      goal.ns = "goal";
      goal.type = marker_shape_;
      goal.action = show;

      // Assign the pose
      goal.pose = c2w_srv_.response.waypoints[i].pose;

      // Assign the scale
      goal.scale.x = 0.3;
      goal.scale.y = 0.06;
      goal.scale.z = 0.06;

      // Assign the color
      goal.color.r = 0.0;
      goal.color.g = 1.0;
      goal.color.b = 0.0;
      goal.color.a = 1.0;

      // Assign the lifetime
      goal.lifetime = ros::Duration();

      goals.markers.push_back(goal);
    }
    // Publish the marker
    goal_markers_publisher_.publish(goals);
  }

public:
  WaypointsParser() {
    // Retrieving ROS parameters
    n_.getParam("waypoints_parser/exec_timeout", exec_timeout_);

    // Set the frame id of the goal
    c_frame_id_ = "map";

    // Set the delay before publisher publish the next goal
    goal_pub_delay_ = ros::Duration(3.0);

    // start the csv_to_waypoints client
    c2w_client_ =
        n_.serviceClient<second_project::CsvToWaypoints>("csv_to_waypoints");

    // start the csv_to_waypoints server
    set_waypoints_server_ = n_.advertiseService(
        "set_waypoints", &WaypointsParser::_setWaypointsCallback, this);
    ROS_INFO("The waypoints setter service is ready.");

    // setup the subscriber to get the status of the move_base
    move_base_status_subscriber_ =
        n_.subscribe("/move_base/status", 1000,
                     &WaypointsParser::_navigationStatusCallback, this);

    // setup the goal publisher
    move_base_goal_publisher_ = n_.advertise<geometry_msgs::PoseStamped>(
        "/move_base_simple/goal", 1000);

    // setup
    goal_markers_publisher_ = n_.advertise<visualization_msgs::MarkerArray>(
        "/waypoints_parser/goal_markers", 1);
  }

  ~WaypointsParser(void) {}
  void _setTimeout(unsigned int new_exec_timeout) {
    exec_timeout_ = new_exec_timeout;
  }

  void _navigationStatusCallback(
      const actionlib_msgs::GoalStatusArray::ConstPtr &mb_status) {
    if (mb_status->status_list.empty())
      status_ = 0;
    else
      status_ = mb_status->status_list.back().status;
  }

  bool _setWaypointsCallback(second_project::SetWaypoints::Request &req,
                             second_project::SetWaypoints::Response &res) {
    // Get the CSV
    c2w_srv_.request.filename = req.filename.c_str();
    if (!c2w_client_.call(c2w_srv_)) {
      // Print out the error and the path that causes the error.
      ROS_ERROR("Falied to parse the CSV file from the path: %s",
                c2w_srv_.request.filename.c_str());
      return false;
    }

    ROS_INFO("Waypoints parsing from CSV file is successful.");

    // Read and set the number of waypoints
    res.num_waypoints = c2w_srv_.response.num_waypoints;
    res.success_waypoints = 0;

    _showMarkers(c2w_srv_.response, visualization_msgs::Marker::ADD);

    // Loop over all the waypoints
    for (unsigned int i = 0; i < res.num_waypoints; i++) {
      // Update the header
      c2w_srv_.response.waypoints[i].header.stamp = ros::Time::now();
      c2w_srv_.response.waypoints[i].header.frame_id = c_frame_id_;

      // Show the information of the current waypoint
      ROS_INFO("Waypoint number %d is published with data x: %.2f y: %.2f.",
               i + 1, c2w_srv_.response.waypoints[i].pose.position.x,
               c2w_srv_.response.waypoints[i].pose.position.y);

      // Publish the goal
      move_base_goal_publisher_.publish(c2w_srv_.response.waypoints[i]);

      // Wait a bit before publishing
      goal_pub_delay_.sleep();

      // Set the star time for the timeout
      ros::Time start_time = ros::Time::now();

      // Polling rate
      ros::Rate r(1);        // 1 Hz
      status_ = 0;
      while (status_ != 3) { // While the robot has not reached the goal
        // Show the remaining time to execute the path
        ROS_INFO(
            "Waiting for the waypoint number %d to complete (time left: %d)",
            i + 1,
            exec_timeout_ - (int)ros::Time::now().toSec() -
                (int)start_time.toSec());

        // if takes too long break
        if ((int)ros::Time::now().toSec() - (int)start_time.toSec() >
            exec_timeout_) {
          res.success_waypoints--; // Remove 1 then add 1 later
          break;
        }
        // Only poll once a second.
        ros::spinOnce(); // Check the move_base status from the callback
        r.sleep();
      }
      status_ = 0;
      res.success_waypoints++;
    }

    if (res.success_waypoints == res.num_waypoints) {
      res.success = true;
    } else {
      res.success = false;
    }

    _showMarkers(c2w_srv_.response, visualization_msgs::Marker::DELETE);

    return true;
  }
};

int main(int argc, char **argv) {
  ros::init(argc, argv, "waypoints_parser");

  WaypointsParser wpp;

  ros::spin();

  return 0;
}
