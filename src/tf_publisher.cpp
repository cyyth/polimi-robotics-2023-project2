#include "ros/ros.h"

// Messages and utils
#include "tf/transform_broadcaster.h"
#include "tf/transform_datatypes.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/Quaternion.h"
#include "nav_msgs/Odometry.h"

class TransformPublisher {
public:
	TransformPublisher() {
		// Retrieving ROS parameters
		n_.getParam("/tf_publisher/is_2d", c_is_2d);

		// Assigning frame IDs
		c_frame_id_ = "odom";
		c_child_frame_id_ = "t265";

		// Subscribe to the odometry data
		sub_ = n_.subscribe("t265/odom", 1000, 
												&TransformPublisher::_callback, this);
	}

private:
	bool c_is_2d = false;
	ros::NodeHandle n_;
	ros::Subscriber sub_;
	std::string c_frame_id_;
	std::string c_child_frame_id_;

	void _callback(const nav_msgs::Odometry::ConstPtr& msg) {
	  // Create a new blank transformation and the transform broadcaster
	  static tf::TransformBroadcaster broadcaster;
	  geometry_msgs::TransformStamped tf_stamped;

	  // Assign the header
	  tf_stamped.header.stamp = msg->header.stamp;
	  tf_stamped.header.frame_id = c_frame_id_;
	  tf_stamped.child_frame_id = c_child_frame_id_;

	  // Since the covariances are blank, so I'm not inheriting them
	  // Assign the pose
	  tf_stamped.transform.translation.x = msg->pose.pose.position.x;
	  tf_stamped.transform.translation.y = msg->pose.pose.position.y;
		if (!c_is_2d) {
	  	tf_stamped.transform.translation.z = msg->pose.pose.position.z;
			tf_stamped.transform.rotation = msg->pose.pose.orientation;
		}
		else {
			tf_stamped.transform.translation.z = 0.0;

			// Convert the quaternion to ratation matrix, then only extract yaw
			tf::Quaternion q_temp(
				msg->pose.pose.orientation.x,
				msg->pose.pose.orientation.y,
				msg->pose.pose.orientation.z,
				msg->pose.pose.orientation.w
			);
			tf::Matrix3x3 rot_m(q_temp);
			double roll,pitch,yaw;
			rot_m.getRPY(roll,pitch,yaw);
			tf_stamped.transform.rotation = tf::createQuaternionMsgFromYaw(yaw);
		}


	  broadcaster.sendTransform(tf_stamped);
	}
};

int main(int argc, char** argv) {
	// Initialize the node
  ros::init(argc, argv, "robot_tf_publisher");

	TransformPublisher tb;

  // Spin
	ros::spin();

	return 1;
}
